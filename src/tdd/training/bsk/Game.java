package tdd.training.bsk;

import java.util.ArrayList;
import java.util.List;

public class Game {
	
	private static final int SIZE = 10;
	private List<Frame> frames;
	private int firstBonusThrow;
	private int secondBonusThrow;
	private boolean firstBonusThrowIsSetted = false;
	private boolean secondBonusThrowIsSetted = false;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new ArrayList<>(SIZE);
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if (frames.size() == SIZE) {
			throw new BowlingException("the list of frames is already full");
		}
		frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if (index < 0) {
			throw new BowlingException("index cannot be less than 0");
		}
		if (index > SIZE-1) {
			throw new BowlingException("index should be less than "+SIZE);
		}
		return frames.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		Frame lastFrame = frames.get(frames.size()-1);
		if ((! lastFrame.isSpare()) && (! lastFrame.isStrike())) {
			throw new BowlingException("last frame isn't Spare or Strike");
		}
		checkInvalidBonusThrow(firstBonusThrow);
		firstBonusThrowIsSetted = true;
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if (! frames.get(frames.size()-1).isStrike()) {
			throw new BowlingException("last frame isn't Strike");
		}
		checkInvalidBonusThrow(secondBonusThrow);
		secondBonusThrowIsSetted = true;
		this.secondBonusThrow = secondBonusThrow;
	}
	
	
	private void checkInvalidBonusThrow(int bonusThrow) throws BowlingException {
		if ((bonusThrow > 10) || (bonusThrow < 0)) {
			throw new BowlingException(bonusThrow + " cannot be a valid bonus throw");
		}
	}
	

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		checkIfBonusThrowsAreSetting();
		int sum = 0;
		int i = 0;
		for (i = 1; i < frames.size(); i++) {
			Frame frame = frames.get(i-1);
			checkStrikeOrSpareFrame(frames.get(i), frame, i);
			sum += frame.getScore();
		}
		
		if (frames.get(i-2).isStrike()) {
			sum += getFirstBonusThrow();
		}
		
		sum += frames.get(i-1).getScore();
		return sum + checkLastFrame(frames.get(i-1));
	}
	
	
	private void checkStrikeOrSpareFrame(Frame currentFrame, Frame frameToCheck, int index) {
		if (frameToCheck.isStrike()) {
			frameToCheck.setBonus(checkConsecutiveStrike(currentFrame, index));
		}
		else if (frameToCheck.isSpare()) {
			frameToCheck.setBonus(currentFrame.getFirstThrow());
		}
	}
	
	
	private int checkConsecutiveStrike(Frame currentFrame, int index) {
		if (currentFrame.isStrike() && (index + 1 < frames.size())) {
			return currentFrame.getFirstThrow() + frames.get(index+1).getFirstThrow();
		}
		return currentFrame.getFirstThrow() + currentFrame.getSecondThrow();
	}
	
	
	private int checkLastFrame(Frame lastFrame) {
		if (lastFrame.isStrike()) {
			return getFirstBonusThrow() + getSecondBonusThrow();
		}
		else if (lastFrame.isSpare()) {
			return getFirstBonusThrow();
		}
		return 0;
	}
	
	
	private void checkIfBonusThrowsAreSetting() throws BowlingException {
		if (frames.get(SIZE-1).isStrike() && (!firstBonusThrowIsSetted || !secondBonusThrowIsSetted)) {
			throw new BowlingException("last frame is a strike and bonus throws aren't setting yet");
		}
		if (frames.get(SIZE-1).isSpare() && !firstBonusThrowIsSetted) {
			throw new BowlingException("last frame is a spare and first bonus throw isn't setting yet");
		}
	}

}
