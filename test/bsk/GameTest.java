package bsk;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {
	
	private Game game;
	
	@Before
	public void setUp() throws Exception {
		game = new Game();
	}
	

	@Test
	public void testGameShouldReturnTheNinthFrameWithFirstThrow2() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		Frame frame = game.getFrameAt(9);
		assertEquals(2, frame.getFirstThrow());
	}
	
	
	@Test(expected = BowlingException.class)
	public void testGameGetFrameAtIndexLessThan0ShouldReturnBowlingException() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		game.getFrameAt(-1);
	}
	
	
	@Test(expected = BowlingException.class)
	public void testGameGetFrameAtIndex10ShouldReturnBowlingException() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		game.getFrameAt(10);
	}
	
	
	@Test(expected = BowlingException.class)
	public void testGameAddFrameForThe11FrameShouldReturnBowlingException() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		game.addFrame(new Frame(1, 2));
	}
	
	
	@Test
	public void testGameCalculateScoreShouldReturn81() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(81, game.calculateScore());
	}
	
	
	@Test
	public void testGameWithASpareFrameCalculateScoreShouldReturn88() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(88, game.calculateScore());
	}
	
	
	@Test
	public void testGameWithAStrikeFrameCalculateScoreShouldReturn94() throws Exception {
		game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(94, game.calculateScore());
	}
	
	
	@Test
	public void testGameWithAStrikeAndASubsequentSpareFrameCalculateScoreShouldReturn103() throws Exception {
		game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(4, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(103, game.calculateScore());
	}
	
	
	@Test
	public void testGameWithTwoSubsequentStrikeFramesCalculateScoreShouldReturn112() throws Exception {
		game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(112, game.calculateScore());
	}
	
	
	@Test
	public void testGameWithTwoSubsequentSpareFramesCalculateScoreShouldReturn98() throws Exception {
		game = new Game();
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(5, 5));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(98, game.calculateScore());
	}
	
	
	@Test
	public void testGameFirstBonusThrowIsSettedWith2ShouldReturn2() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		game.setFirstBonusThrow(2);
		assertEquals(2, game.getFirstBonusThrow());
	}
	
	
	@Test(expected = BowlingException.class)
	public void testGameSettingFirstBonusThrowButLastFrameIsNotSpareOrStrikeAndShouldReturnBowlingException() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		game.setFirstBonusThrow(2);
	}
	
	
	@Test(expected = BowlingException.class)
	public void testGameSettingSecondBonusThrowButLastFrameIsSpareAndNotIsStrikeAndShouldReturnBowlingException() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		game.setSecondBonusThrow(2);
	}
	
	
	@Test
	public void testGameLastFrameSpareAndBonusThrowIs7AndCalculateScoreShouldReturn90() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		game.setFirstBonusThrow(7);
		assertEquals(90, game.calculateScore());
	}
	
	
	@Test
	public void testGameSecondBonusThrowIsSettedWith3ShouldReturn3() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		game.setSecondBonusThrow(3);
		assertEquals(3, game.getSecondBonusThrow());
	}
	
	
	@Test(expected = BowlingException.class)
	public void testGameSecondBonusThrowIsSettedWith11ButShouldReturnBowlingException() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		game.setSecondBonusThrow(11);
	}
	
	
	@Test
	public void testGameLastFrameStrikeAndBonusThrowsAre7And2AndCalculateScoreShouldReturn92() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		assertEquals(92, game.calculateScore());
	}
	
	
	@Test
	public void testGameBestScoreCalculateScoreShouldReturn300() throws Exception {
		game = new Game();
		for (int i = 0; i < 10; i++) {
			game.addFrame(new Frame(10, 0));
		}
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		assertEquals(300, game.calculateScore());
	}
	
	
	@Test(expected = BowlingException.class)
	public void testGameLastFrameStrikeAndBonusThrowsAreNotSettedShouldReturnBowlingException() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		game.calculateScore();
	}
	
	
	@Test(expected = BowlingException.class)
	public void testGameLastFrameStrikeAndSecondBonusThrowIsNotSettedShouldReturnBowlingException() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		game.setFirstBonusThrow(4);
		game.calculateScore();
	}
	
	
	@Test(expected = BowlingException.class)
	public void testGameLastFrameStrikeAndFirstBonusThrowIsNotSettedShouldReturnBowlingException() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		game.setSecondBonusThrow(4);
		game.calculateScore();
	}
	
	
	@Test(expected = BowlingException.class)
	public void testGameLastFrameSpareAndFirstBonusThrowIsNotSettedShouldReturnBowlingException() throws Exception {
		game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		game.calculateScore();
	}
	

}
