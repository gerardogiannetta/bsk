package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testFrameWith2PinsDownInTheFirstThrowShouldReturn2ForTheFirstThrow() throws Exception {
		Frame frame = new Frame(2, 4);
		assertEquals(2, frame.getFirstThrow());
	}
	
	
	@Test
	public void testFrameWith4PinsDownInTheSecondThrowShouldReturn4ForTheSecondThrow() throws Exception {
		Frame frame = new Frame(2, 4);
		assertEquals(4, frame.getSecondThrow());
	}
	
	
	@Test(expected = BowlingException.class)
	public void testFrameWith11PinsDownInTheFirstThrowShouldReturnBowlingException() throws Exception {
		new Frame(11, 4);
	}
	
	
	@Test(expected = BowlingException.class)
	public void testFrameWithLessThan0PinsDownInTheSecondThrowShouldReturnBowlingException() throws Exception {
		new Frame(2, -1);
	}
	
	
	@Test
	public void testGetScoreOfFrameWith2And6PinsDownInTheTwoThrowsShouldReturn8() throws Exception {
		Frame frame = new Frame(2, 6);
		assertEquals(8, frame.getScore());
	}
	
	
	@Test
	public void testFrameSetBonusOf3ShouldReturnBonusOf3() throws Exception {
		Frame frame = new Frame(8, 2);
		frame.setBonus(3);
		assertEquals(3, frame.getBonus());
	}
	
	
	@Test
	public void testFrameIsSpareShouldReturnTrue() throws Exception {
		Frame frame = new Frame(8, 2);
		assertTrue(frame.isSpare());
	}
	
	
	@Test
	public void testSpareFrameShouldReturnScorePlusBonusOf3() throws Exception {
		Frame frame = new Frame(8, 2);
		frame.setBonus(3);
		assertEquals(13, frame.getScore());
	}
	
	
	@Test
	public void testFrameIsStrikeShouldReturnTrue() throws Exception {
		Frame frame = new Frame(10, 0);
		assertTrue(frame.isStrike());
	}
	
	
	@Test(expected = BowlingException.class)
	public void testFrameStrikeWithSecondThrowGreater0ThanShouldReturnBowlingException() throws Exception {
		new Frame(10, 3);
	}
	
	
	@Test
	public void testStrikeFrameShoudlReturnScorePlusBonusOf7() throws Exception {
		Frame frame1 = new Frame(10, 0);
		frame1.setBonus(5+2);
		assertEquals(17, frame1.getScore());
	}


}
